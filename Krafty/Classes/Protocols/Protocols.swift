//
//  Protocols.swift
//  ShaeFoodDairy
//
//  Created by Mac on 17/03/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit


protocol SideMenuDelegate {
    func optionButtonTapped(itemIndex: Int)
}

protocol RegisterComplaintDelegate {
    func registerComplaint(orderId: Int, driverId: Int, contractorName: String)
}

protocol ViewCardsDelegate {
    func slideToNextCard(currentCardIndex: Int)
    func slideToStart()
    func minimizeCard()
    func sendTapped(message: String)
}

protocol OrderCompleteDelegate {
    func showRatingCard()
}
