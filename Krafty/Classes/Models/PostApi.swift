//
//  PostApis.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias PostApiCompletionHandler = (_ data: PostApi?,_ error: NSError?, _ statusCode: Int) -> Void

class PostApi: Mappable {
    
    var message = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        message                <- map["data"]
    }
    
    
    //MARK: - CodeVerificationViewController
    class func verifyPin(email: String, pin: String, completion:@escaping SignUpCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.verifyPin(email: email, pin: pin) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Profile>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Profile>().map(JSON: [:])!, nil, "")
                }
                
            }  else {
                print(error?.localizedDescription ?? "")
                completion(Mapper<Profile>().map(JSON: [:])!, error, "There was an error. Pleas try again.")
            }
        }
    }
    
    class func resendVerificationPin(email: String, completion:@escaping PostApiCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.resendVerificationPin(email: email) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            completion(Mapper<PostApi>().map(JSON: [:]),error, statusCode)
        }
    }
    
    
    //MARK: - ForgotPasswordViewController
    class func forgotPassword(email: String, completion:@escaping PostApiCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.forgotPassword(email: email) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            completion(Mapper<PostApi>().map(JSON: [:]),error, statusCode)
        }
    }
    
    
    //MARK: - ChangePasswordViewController
    class func updatePassword(token: String, password: String, completion:@escaping PostApiCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.updatePassword(token: token, password: password) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            completion(Mapper<PostApi>().map(JSON: [:]),error, statusCode)
        }
    }
    
    class func changePassword(oldPassword: String, newPassword: String, completion:@escaping (_ message: String?,_ error: Error?) -> ()) {
        
        Utility.showLoading()
        
        APIClient.shared.changePassword(oldPassword: oldPassword, newPassword: newPassword) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
                completion(data as? String, error)
            }
            completion(data as? String, error)
        }
    }
    
    
    //MARK: - SettingsViewController
    class func setNotificationStatus(status: Int, completion:@escaping PostApiCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.setNotificationStatus(status: status) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            completion(Mapper<PostApi>().map(JSON: [:]),error, statusCode)
        }
    }
    
    
    //MARK: - ComplaintViewController
    class func registerComplaint(orderId: Int, driverId: Int, description: String, completion:@escaping PostApiCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.registerComplaint(orderId: orderId, driverId: driverId, description: description) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            completion(Mapper<PostApi>().map(JSON: [:]),error, statusCode)
        }
    }
    
    
    //MARK: - MapsViewController
    class func cancelOrder(orderId: Int, driverId: String, completion:@escaping PostApiCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.cancelOrder(orderId: orderId, driverId: driverId) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            completion(Mapper<PostApi>().map(JSON: [:]),error, statusCode)
        }
    }
    
    class func rateDriver(driverId: Int, performance: String, stars: Int, message: String, completion:@escaping PostApiCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.rateDriver(driverId: driverId, performance: performance, stars: stars, message: message) { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            completion(Mapper<PostApi>().map(JSON: [:]),error, statusCode)
        }
    }
    
    
    //MARK: - SideMenuViewController
    class func logout(completion:@escaping PostApiCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.logout() { (data, error, statusCode)  in
            
            Utility.hideLoading()
            
            if error != nil {
                print(error?.localizedDescription ?? "")
            }
            completion(Mapper<PostApi>().map(JSON: [:]),error, statusCode)
        }
    }
    
    
    //MARK: - SignIn & SignUp View Controllers
    class func updateFCMToken(token:String, completion: @escaping (_ message:String?)->Void) {
        Utility.showLoading()
        
        APIClient.shared.updateFCMToken(fcmToken: token) { (data, error, statusCode) in
            Utility.hideLoading()
            if error == nil {
                let message = data as? String ?? ""
                completion(message)
                
            } else {
                completion(nil)
            }
        }
    }
    
    class func clearNotifiaction(completion: @escaping (NSError?) -> Void) {
        
        APIClient.shared.clearNotifiaction { (data, error, statusCode)  in
            
            if error != nil {
                print(error?.localizedDescription ?? "")
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    class func readNotifiaction(notificationID: Int, completion: @escaping (NSError?) -> Void) {
        
        APIClient.shared.readNotifiaction(noificationID: notificationID) { (data, error, statusCode)  in
            
            if error != nil {
                print(error?.localizedDescription ?? "")
                completion(error)
                return
            }
            completion(nil)
        }
    }
}
