//
//  User.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper
import SocketIO

class User {
    static let shared = User()
    var currentJob = Mapper<Job>().map(JSON: [:])!
    var contractor = Mapper<Contractor>().map(JSON: [:])
    var profile = Mapper<Profile>().map(JSON: [:])
    var placeOrder = PlaceOrder()
    var socket:SocketIOClient!
    var services = Mapper<Services>().map(JSON: [:])!
    
}

class PlaceOrder {
    var driverId = 1 // static for now. will be dynamic after socket implementation
    var serviceId = -1
    var serviceImageUrl = ""
    var address = ""
    var location = ""
    var description = ""
    var serviceType = ""
    var images = [ #imageLiteral(resourceName: "ic_add") ]
}
