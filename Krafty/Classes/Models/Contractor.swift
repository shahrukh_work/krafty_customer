//
//  Contractor.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/5/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class Contractor: Mappable {
    var location = Mapper<ContractorLocation>().map(JSON: [:])
    var image = ""
    var name = ""
    var id = -1
    var stars = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        image <- map["driver_image"]
        name <- map["driver_name"]
        id <- map["id"]
        stars <- map["stars"]
    }
}


class ContractorLocation: Mappable {
    
    var latitude = 0.0
    var longitude = 0.0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
