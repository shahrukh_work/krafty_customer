//
//  DriverLocation.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 6/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class Location: Mappable {
    
    var latitude = 0.0
    var longitude = 0.0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
