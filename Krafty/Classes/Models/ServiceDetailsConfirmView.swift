//
//  ServiceDetailsConfirmView.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/5/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ServiceDetailsConfirmView: UIView {

    
    //MARK: - Variables
    var view: UIView!
    var data = [ServiceTypeData]()
    var selectionsMade = false
    var images = [ #imageLiteral(resourceName: "ic_add") ]
    var parentController: UIViewController?
    var notesAdded = false
    var delegate: ViewCardsDelegate?
    var index = 0
    
    
    //MARK: - Outlets
    @IBOutlet weak var contractorImageView: UIImageView!
    @IBOutlet weak var contractorTypeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - View Setup
    func setup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupView()
        loadData()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func setupView() {
        messageTextView.text = "Add notes of your issue"
        messageTextView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        messageTextView.delegate = self
        messageTextView.textContainerInset = UIEdgeInsets(top: 9, left: 13, bottom: 17, right: 6)
        containerView.cornerRadius = 9
        containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        optionsCollectionView.delegate = self
        optionsCollectionView.dataSource = self
    }
    
    
    //MARK: - Private Method
    private func loadData() {
        data.append(ServiceTypeData(title: "Option 1"))
        data.append(ServiceTypeData(title: "Option 2"))
        data.append(ServiceTypeData(title: "Option 3"))
        data.append(ServiceTypeData(title: "Option 4"))
        data.append(ServiceTypeData(title: "Option 5"))
        data.append(ServiceTypeData(title: "Option 6"))
        optionsCollectionView.reloadData()
    }
    
    private func updateView() {
        selectionsMade = false
        
        data.forEach { (dta) in
            selectionsMade = selectionsMade || (dta.isSelected && notesAdded && images.count > 1)
        }
        
        if selectionsMade {
            nextButton.alpha = 1
            nextButton.setTitle("Book", for: .normal)
            
        } else {
            nextButton.alpha = 0.6
            nextButton.setTitle("Next", for: .normal)
        }
    }
    
    
    //MARK: - Actions
    @IBAction func minimizeButtonTapped(_ sender: Any) {
        delegate?.minimizeCard()
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        if nextButton.titleLabel?.text == "Book" {
            
            if messageTextView.contentSize.height < messageTextView.frame.height {
                messageTextViewHeightConstraint.constant = messageTextView.contentSize.height
                layoutIfNeeded()
            }
            messageTextView.isEditable = false
            messageTextView.scrollRangeToVisible(NSMakeRange(0,0))
            images.remove(at: 0)
            imagesCollectionView.reloadData()
            
            let tempDataList = data
            data.removeAll()
            tempDataList.forEach{ (dta) in
                if dta.isSelected {data.append(dta)}
            }
            optionsCollectionView.reloadData()
            nextButton.setTitle("Confirm", for: .normal)
            
        } else if nextButton.titleLabel?.text == "Confirm" {
            User.shared.placeOrder.description = messageTextView.text
            User.shared.placeOrder.images.removeAll()
            User.shared.placeOrder.images.append(contentsOf: images)
            delegate?.slideToNextCard(currentCardIndex: index)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        delegate?.slideToStart()
    }
}


//MARK: - CollectionView Datasource & Delegate
extension ServiceDetailsConfirmView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == optionsCollectionView {
            return data.count
        }
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == optionsCollectionView {
            let cell = collectionView.register(ServiceTypeCollectionViewCell.self, indexPath: indexPath)
            cell.configure(data: data[indexPath.row])
            return cell
        }
        
        let cell = collectionView.register(ImageCollectionViewCell.self, indexPath: indexPath)
        
        if indexPath.row == 0 {
            cell.config(image: images[indexPath.row], isImagePicker: true)
        
        } else {
            cell.config(image: images[indexPath.row], isImagePicker: false)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == optionsCollectionView {
            let label = UILabel(frame: CGRect.zero)
            label.font = UIFont(name: "Poppins-Regular", size: 14)
            label.text = data[indexPath.row].title
            label.sizeToFit()
            return CGSize(width: label.frame.width + 10, height: 23)
        }
        return CGSize(width: 56, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == optionsCollectionView {
            data[indexPath.row].isSelected.toggle()
            collectionView.reloadData()
            updateView()
            return
        }
        
        if indexPath.row == 0 { // image picker tapped
            
            ImagePickerManager().pickImage(parentController!){ image in
                self.images.append(image)
                self.imagesCollectionView.reloadData()
                self.updateView()
            }
        }
    }
}


//MARK: - TextView Delegate
extension ServiceDetailsConfirmView: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            textView.text = "Add notes of your issue"
            textView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
            notesAdded = false
        
        } else {
            notesAdded = true
        }
        updateView()
    }
}
