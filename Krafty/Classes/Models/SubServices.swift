//
//  SubServices.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 6/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias SubServicesCompletionHandler = (_ data: SubServices?,_ error: NSError?,_ message: String) -> Void

class SubServices: Mappable {
    var subServicesList = [SubService]()
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        subServicesList <- map["sub_services"]
    }
    
    class func getSubServices(serviceId: Int, completion: @escaping SubServicesCompletionHandler) {
        
        Utility.showLoading()
        
        APIClient.shared.getSubServices(serviceId: serviceId) { (data, error, message) in
        
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<SubServices>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<SubServices>().map(JSON: [:])!, nil, "")
                }
                
            }  else {
                print(error?.localizedDescription ?? "")
                completion(Mapper<SubServices>().map(JSON: [:])!, error, "There was an error. Pleas try again.")
            }
        }
    }
}

class SubService: Mappable {
    var id = ""
    var name = ""
    var serviceId = ""
    var isSelected = false
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        serviceId <- map["service_id"]
    }
}
