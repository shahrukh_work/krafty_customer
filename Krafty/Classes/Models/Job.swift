//
//  Job.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 6/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class Job: Mappable {
    
    var driverImage = ""
    var driverName = ""
    var id = -1
    var driverId = 0
    var customerId = -1
    var orderAddress = ""
    private var orderImageString = ""
    private var latLongString = ""
    private var userOptionsString = ""
    var orderImages = [String]()
    var userOptions = [String]()
    var orderDetails = ""
    var lat = ""
    var long = ""
    var stars = 0.0
    var status = 0
    var serviceType = -1
    var serviceTypeString = ""
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        driverId          <- map["driver_id"]
        driverImage       <- map["driver_image"]
        driverName        <- map["driver_name"]
        id                  <- map["id"]
        customerId          <- map["customer_id"]
        orderAddress        <- map["order_address"]
        orderImageString    <- map["order_images"]
        latLongString       <- map["order_location"]
        userOptionsString   <- map["user_options"] //TODO: - To be updated key missing from backend
        orderDetails        <- map["order_detail"]
        stars               <- map["stars"]
        status              <- map["status"]
        serviceType         <- map["service-type"]
        postMap()
    }
    
    func postMap() {
        orderImages = orderImageString.components(separatedBy: ",")
        userOptions = userOptionsString.components(separatedBy: ",")
        let latLong = latLongString.components(separatedBy: ",")
        lat = latLong.first ?? ""
        long = latLong.last ?? ""
    }
}
