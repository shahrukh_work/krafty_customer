//
//  Orders.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 6/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias OrdersCompletionHandler = (_ data: Orders?,_ error: NSError?,_ message: String) -> Void

class Orders: Mappable {
    var currentOrdersList = [Order]()
    var completedOrdersList = [Order]()
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        currentOrdersList <- map["current_order"]
        completedOrdersList <- map["order_history"]
    }
    
    class func getCurrentOrders(offset: Int,_ completion: @escaping OrdersCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getCurrentOrders(offset: offset) { (data, error, message) in
            
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<Orders>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Orders>().map(JSON: [:]), nil, "")
                }
            
            } else {
                completion(Mapper<Orders>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
    
    class func getCompletedOrders(offset: Int,_  completion: @escaping OrdersCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getCompletedOrders(offset: offset) { (result, error, message) in
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<Orders>().map(JSONObject: result) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<Orders>().map(JSON: [:]), nil, "")
                }
            
            } else {
                completion(Mapper<Orders>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
}

class Order: Mappable {
    var id = -1
    var driverName = ""
    var customerName = ""
    var driverLocation = ""
    var estimatedDistance = ""
    var customerAddress = ""
    var customerLocation = ""
    var description = ""
    var serviceType = ""
    var serviceName = ""
    var status = -1
    var startDateTime = ""
    var endDateTime = ""
    var createdBy = ""
    var createdAt = ""
    var updatedAt = ""
    var images = ""
    var imageUrls = [String]() // this property is being used in controller after splittings 'images' property
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        driverName <- map["driver_name"]
        customerName <- map["customer_name"]
        driverLocation <- map["driver_location"]
        estimatedDistance <- map["estimated_distance"]
        customerAddress <- map["customer_address"]
        customerLocation <- map["customer_location"]
        description <- map["order_detail"]
        serviceType <- map["service_type"]
        status <- map["status"]
        startDateTime <- map["start_date_time"]
        endDateTime <- map["end_date_time"]
        createdBy <- map["created_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        images <- map["images"]
        serviceName <- map["service_name"]
    }
}
