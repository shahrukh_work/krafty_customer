//
//  OrderChatHistory.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 6/3/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ChatCompletionHandler = (_ data: ChatHistory?,_ error: NSError?,_ message: String) -> Void

class ChatHistory: Mappable {
    var chatHistory = [Chat]()
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        chatHistory <- map["chat_history"]
    }
    
    class func loadChat(orderId: Int, completion: @escaping ChatCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getOrderChatHistory(orderId: orderId) { (data, error, message) in
            
            Utility.hideLoading()
            
            if error == nil {
                if let data = Mapper<ChatHistory>().map(JSONObject: data) {
                    completion(data, nil, "")
                    
                } else {
                    completion(Mapper<ChatHistory>().map(JSON: [:]), nil, "")
                }
            
            } else {
                completion(Mapper<ChatHistory>().map(JSON: [:])!, error, "Error!")
            }
        }
    }
}

class Chat: Mappable {
    var id = -1
    var orderId = -1
    var message = ""
    var sendBy = -1 // 1 -> Customer, 2 -> Driver
    var createdAt = ""
    var updatedAt = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        orderId <- map["order_id"]
        message <- map["message"]
        sendBy <- map["send_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
    }
}
