
import UIKit
import Alamofire
import ObjectMapper

class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

let APIClientDefaultTimeOut = 40.0

class APIClient: APIClientHandler {
    
    fileprivate var clientDateFormatter: DateFormatter
    var isConnectedToNetwork: Bool?
    
    static var shared: APIClient = {
        let baseURL = URL(fileURLWithPath: APIRoutes.baseUrl)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIClientDefaultTimeOut
        
        let instance = APIClient(baseURL: baseURL, configuration: configuration)
        
        return instance
    }()
    
    // MARK: - init methods
    
    override init(baseURL: URL, configuration: URLSessionConfiguration, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        clientDateFormatter = DateFormatter()
        
        super.init(baseURL: baseURL, configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        //        clientDateFormatter.timeZone = NSTimeZone(name: "UTC")
        clientDateFormatter.dateFormat = "yyyy-MM-dd" // Change it to desired date format to be used in All Apis
    }
    
    
    // MARK: Helper methods
    
    func apiClientDateFormatter() -> DateFormatter {
        return clientDateFormatter.copy() as! DateFormatter
    }
    
    fileprivate func normalizeString(_ value: AnyObject?) -> String {
        return value == nil ? "" : value as! String
    }
    
    fileprivate func normalizeDate(_ date: Date?) -> String {
        return date == nil ? "" : clientDateFormatter.string(from: date!)
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getUrlFromParam(apiUrl: String, params: [String: AnyObject]) -> String {
        var url = apiUrl + "?"
        
        for (key, value) in params {
            url = url + key + "=" + "\(value)&"
        }
        url.removeLast()
        return url
    }
    
    
    //MARK: - SignInViewController
    @discardableResult
    func signIn(email: String, password: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let headers = ["email": email, "password": password]
           return sendRequest(APIRoutes.signin, parameters: nil, httpMethod: .post, headers:headers  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func updateFCMToken(fcmToken:String, _ completionBlock:@escaping APIClientCompletionHandler)->Request {
        let tokenString = "bearer " + (User.shared.profile?.token ?? "")
        let params = ["fcm_token": fcmToken] as [String:AnyObject]
        return sendRequest(APIRoutes.updateFCMToken, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": tokenString], completionBlock: completionBlock)
    }
    
    
    // MARK: - SignUpViewController
    @discardableResult
    func signUp(username:String, firstName: String, lastName: String, email: String, password: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["username": username, "first_name": firstName, "last_name": lastName, "email":email, "password":password] as [String:AnyObject]
           return sendRequest(APIRoutes.create, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - CodeVerificationViewController
    @discardableResult
    func verifyPin(email: String, pin: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["email": email, "pin": pin] as [String:AnyObject]
           return sendRequest(APIRoutes.verifyPin, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func resendVerificationPin(email: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["email": email] as [String:AnyObject]
           return sendRequest(APIRoutes.resendVerificationPin, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - ForgotPasswordViewController
    @discardableResult
    func forgotPassword(email: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["user_type": "1", "email": email] as [String:AnyObject]
           return sendRequest(APIRoutes.forgotPassword, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - ChangePasswordViewController
    @discardableResult
    func updatePassword(token: String, password: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["user_type": "1", "password": password] as [String:AnyObject]
           return sendRequest(APIRoutes.updatePassword, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(token)"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func changePassword(oldPassword: String, newPassword: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["user_type": "1", "old_password": oldPassword, "new_password": newPassword] as [String:AnyObject]
        return sendRequest(APIRoutes.changePassword, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - SettingsViewController
    @discardableResult
    func setNotificationStatus(status: Int,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["status": status] as [String:AnyObject]
        return sendRequest(APIRoutes.setNotificationStatus, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    func getNotificationStatus( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getNotificationStatus, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - ComplaintViewController
    @discardableResult
    func registerComplaint(orderId: Int, driverId: Int, description: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["order_id": orderId, "driver_id": driverId, "description": description] as [String:AnyObject]
        return sendRequest(APIRoutes.registerComplaint, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - MyOrderViewController
    @discardableResult
    func getCurrentOrders(offset: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getCurrentOrders, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func getCompletedOrders(offset: Int,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getCompletedOrders, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - OrderHistoryViewController
    @discardableResult
    func getOrderDetails(orderId: Int,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["order_id": orderId] as [String:AnyObject]
        return sendRequest(APIRoutes.getOrderDetails, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer " + User.shared.profile!.token]  , completionBlock: completionBlock)
    }
    
    
    //MARK: - SideMenuViewController
    @discardableResult
    func logout( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.logout, parameters: nil, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer " + User.shared.profile!.token]  , completionBlock: completionBlock)
    }
    
    
    // MARK: - EditProfileViewController
    @discardableResult
    func getUserDetails(userId: Int,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["id": userId] as [String:AnyObject]
        return sendRequest(APIRoutes.getUserDetials, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer " + (User.shared.profile?.token ?? "")]  , completionBlock: completionBlock)
    }
    
    func updateProfileData(profile: ProfileData, profileImage: UIImage?, completion: @escaping APIClientCompletionHandler) {
        
        let tokenString = "bearer " + (User.shared.profile?.token ?? "")
        let header: HTTPHeaders = ["Content-Type":  "application/x-www-form-urlencoded", "Authorization":tokenString]
        
        let parameters = ["id": String(profile.id), "username": profile.userName, "first_name": profile.firstName, "last_name": profile.lastName, "dob": profile.dob, "email": profile.email, "gender": profile.gender, "phone": profile.phone]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            if profileImage != nil {
                //let rotatedImage = profileImage?.rotate(radians: .pi)
                multipartFormData.append((profileImage?.jpeg(.medium))!, withName: "image", fileName: "a66.jpg", mimeType: "image/png")
            }
        }, usingThreshold: 0, to: APIRoutes.baseUrl + APIRoutes.updateProfile, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _ , _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success:
                        self.showRequestDetailForSuccess(responseObject: response)
                        completion(response.result.value as AnyObject?, nil, 0)
                        break
                    case .failure(let error):
                        self.showRequestDetailForFailure(responseObject: response)
                        completion(nil, error as NSError?, 0)
                        break
                    }
                }
            case .failure:
                break
            }
        })
    }
    
    
    //MARK: - MapsViewController
    @discardableResult
    func getServicesList( _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getServicesList, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \((User.shared.profile?.token ?? ""))"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func getSubServices(serviceId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["service_id": serviceId] as [String:AnyObject]
        return sendRequest(APIRoutes.getSubServices, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer \((User.shared.profile?.token ?? ""))"]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func cancelOrder(orderId: Int, driverId: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["order_id": orderId, "driver_id": driverId] as [String:AnyObject]
        return sendRequest(APIRoutes.cancelOrder, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer " + (User.shared.profile?.token ?? "")]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func rateDriver(driverId: Int, performance: String, stars: Int, message: String,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["driver_id": driverId, "performance": performance, "stars": stars, "message": message ] as [String:AnyObject]
        return sendRequest(APIRoutes.rateDriver, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "bearer " + (User.shared.profile?.token ?? "")]  , completionBlock: completionBlock)
    }
    
    @discardableResult
    func getOrderChatHistory(orderId: Int,  _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["order_id": orderId] as [String:AnyObject]
        return sendRequest(APIRoutes.getOrderChatHistory, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "bearer " + (User.shared.profile?.token ?? "")]  , completionBlock: completionBlock)
    }
    
    func placeOrder(placedOrder: PlaceOrder, completion: @escaping APIClientCompletionHandler) {
        
        let tokenString = "bearer " + (User.shared.profile?.token ?? "")
        let header: HTTPHeaders = ["Content-Type":  "application/x-www-form-urlencoded", "Authorization":tokenString]
        
        let parameters = ["address": placedOrder.address, "location": placedOrder.location, "order_detail": placedOrder.description, "service_type": String(placedOrder.serviceId)]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            
            for image in placedOrder.images {
                multipartFormData.append(image.jpegData(compressionQuality: 0.2)!, withName: "image", fileName: "a66.jpg", mimeType: "image/png")
            }
            
        }, usingThreshold: 0, to: APIRoutes.baseUrl + APIRoutes.placeOrder, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _ , _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success:
                        self.showRequestDetailForSuccess(responseObject: response)
                        if let data = response.result.value as? [String:Any] {
                            print(data)
                            
                            if let response = data["data"] as? [String:AnyObject] {
                                let orderId = response["order_id"] as! Int
                                print(orderId)
                                User.shared.currentJob.id = orderId
                            }
                        }
                        completion(response.result.value as AnyObject?, nil, 0)
                        break
                    case .failure(let error):
                        self.showRequestDetailForFailure(responseObject: response)
                        completion(nil, error as NSError?, 0)
                        break
                    }
                }
            case .failure:
                break
            }
        })
    }
    
    @discardableResult
    func getNotifications(completion: @escaping APIClientCompletionHandler) -> Request {
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.getNotifications, method: .get, parameters: nil, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func clearNotifiaction(completion: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.clearNotifications, parameters: nil, httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    @discardableResult
    func readNotifiaction(noificationID: Int, completion: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.readNotifications, parameters: ["notification_id": noificationID] as [String: AnyObject], httpMethod: .post, headers: ["Content-Type":  "application/json", "Authorization": "bearer \(User.shared.profile?.token ?? "")"], completionBlock: completion)
    }
    
    //
    //extension UIImage {
    //    func rotate(radians: Float) -> UIImage? {
    //        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
    //        // Trim off the extremely small float value to prevent core graphics from rounding it up
    //        newSize.width = floor(newSize.width)
    //        newSize.height = floor(newSize.height)
    //
    //        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
    //        let context = UIGraphicsGetCurrentContext()!
    //
    //        // Move origin to middle
    //        context.translateBy(x: newSize.width/2, y: newSize.height/2)
    //        // Rotate around middle
    //        context.rotate(by: CGFloat(radians))
    //        // Draw the image at its center
    //        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
    //
    //        let newImage = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        return newImage
    //    }
}

