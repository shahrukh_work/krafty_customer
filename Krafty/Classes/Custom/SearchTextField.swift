//
//  SearchTextField.swift
//  Krafty
//
//  Created by Bilal Saeed on 5/5/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SearchTextField: UITextField {
    
    
    //MARK: - Variables
    var padding = UIEdgeInsets(top: 0, left: 48, bottom: 0, right: 5)
    
    //MARK: - SkyFloatingLabelTextField Methods
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.backgroundColor = UIColor.white
        self.borderStyle = .none
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
        self.clipsToBounds = true
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    //MARK: - Public Methods
    
    func setLeftImage(image:UIImage) {
        self.leftViewMode = .always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 17))
        let imageView = UIImageView(frame: CGRect(x: 15, y: 0, width: 17, height: 17))
        imageView.image = image
        view.addSubview(imageView)
        self.leftView = view
    }
    
    func setPadding(insets: UIEdgeInsets) {
        self.padding = insets
        self.layoutIfNeeded()
    }
    
}
