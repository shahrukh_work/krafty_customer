//
//  SettingsViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/24/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        NotificationStatus.getNotificationStatus() { (data,error,message) in
            
            if error == nil {
                if data?.status == 0 {
                    self.notificationsSwitch.isOn = false
                
                } else {
                    self.notificationsSwitch.isOn = true
                }
            
            } else {
                self.showOkAlert("Error! Couldn't get notification status of the app.")
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func editProfileButtonTapped(_ sender: Any) {
        let controller = EditProfileViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func notificationSwitchValueChanged(_ sender: Any) {
        var status = 0
        
        if notificationsSwitch.isOn {
           status = 1
        }
        
        PostApi.setNotificationStatus(status: status) { (data,error, statusCode) in
            
            if error == nil {
                
                if status == 0 {
                    self.showOkAlert("Your app will not receive notifications now.")
                
                } else {
                    self.showOkAlert("Your app will receive notifications now.")
                }
                
            } else {
                self.showOkAlert("Error! Please try again.")
            }
        }
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
    }
    
    @IBAction func twitterButtonTapped(_ sender: Any) {
    }
    
    @IBAction func instagramButtonTapped(_ sender: Any) {
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
