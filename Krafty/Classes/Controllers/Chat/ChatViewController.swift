//
//  ChatViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var bottomMessageView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendMessageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: - Variables
    var messages = ["Hey! How are you doing?", "I’m ok thanks for asking and you? It's been a long time since we've seen each other.", "It’s going well. Yeah I was busy"]

    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        bottomMessageView.layer.cornerRadius = bottomMessageView.frame.height/2
        tableView.delegate = self
        tableView.dataSource = self
        messageTextView.delegate = self
        messageTextView.text = "Type"
        messageTextView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        tableView.setContentOffset(CGPoint(x: 0, y: CGFloat.greatestFiniteMagnitude), animated: false)
    }
    
    
    //MARK: - Actions
    @IBAction func sendButonPressed(_ sender: Any) {
        
        if messageTextView.text != "" {
            messages.append(messageTextView.text)
            messageTextView.text = ""
            messageTextViewHeightConstraint.constant = 37
            tableView.reloadData()
            tableView.setContentOffset(CGPoint(x: 0, y: CGFloat.greatestFiniteMagnitude), animated: false)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - TableView Delegate & DataSource
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.register(DateSectionHeaderTableViewCell.self, indexPath: IndexPath(row: 0, section: section ))
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.register(IncomingMessageTableViewCell.self, indexPath: indexPath)
            cell.configCell(message: messages[indexPath.row])
            return cell
        
        } else {
            let cell = tableView.register(OutgoingMessageTableViewCell.self, indexPath: indexPath)
            cell.configCell(message: messages[indexPath.row])
            return cell
        }
    }
}


//MARK: - TextView Delegate
extension ChatViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            messageTextViewHeightConstraint.constant = 37
            textView.text = "Type"
            textView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let minTextViewHeight: CGFloat = 37
        let maxTextViewHeight: CGFloat = 111

        var height = ceil(textView.contentSize.height) // ceil to avoid decimal

        if (height < minTextViewHeight) {
            height = minTextViewHeight
        }
        
        if (height > maxTextViewHeight) {
            height = maxTextViewHeight
        }

        if height != messageTextViewHeightConstraint.constant {
            messageTextViewHeightConstraint.constant = height
            textView.setContentOffset(.zero, animated: false) // scroll to top to avoid "wrong contentOffset" artefact when line count changes
        }
    }
}
