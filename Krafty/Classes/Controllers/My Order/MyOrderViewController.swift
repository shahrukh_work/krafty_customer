//
//  MyOrderViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class MyOrderViewController: UIViewController {
    
    
    //MARK: - Variables
    var showCurrentOrders = false
    var currentOrdersList = [Order]()
    var completedOrdersList = [Order]()
    var currentOrderPage = 0
    var completedOrderPage = 0

    
    //MARK: - Outlets
    @IBOutlet weak var curvedView: UIView!
    @IBOutlet weak var historyDownArrowImageView: UIImageView!
    @IBOutlet weak var currentOrderButton: UIButton!
    @IBOutlet weak var currentOrderDownArrowImageView: UIImageView!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        curvedView.layer.cornerRadius = 16
        curvedView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        tableView.delegate = self
        tableView.dataSource = self
        getCompletedOrders()
    }
    
    
    //MARK: - Private Methods
    private func getCurrentOrders() {

        Orders.getCurrentOrders(offset: currentOrderPage) {[weak self] (data, error, message) in
            self?.currentOrdersList.removeAll()
            self?.showCurrentOrders = true
            
            if error == nil {
                self?.currentOrdersList.append(contentsOf: data?.currentOrdersList ?? [])
                
                if self?.currentOrdersList.count == 0 {
                    self?.showOkAlert("There are no current orders.")
                    self?.tableView.reloadData()
                
                } else {
                    self?.tableView.reloadData()
                }
                
            } else {
                self?.showOkAlert("There was an error. Please try again.")
            }
        }
    }
    
    private func getCompletedOrders() {
        
        Orders.getCompletedOrders(offset: completedOrderPage) {[weak self] (data, error, message) in
            self?.completedOrdersList.removeAll()
            self?.showCurrentOrders = false

            if error == nil {
                self?.completedOrdersList.append(contentsOf: data?.completedOrdersList ?? [])
                
                if self?.completedOrdersList.count == 0 {
                    self?.showOkAlert("There are no completed orders.")
                
                } else {
                    self?.tableView.reloadData()
                }
                
            } else {
                self?.showOkAlert("There was an error. Please try again.")
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func historyButtonTapped(_ sender: Any) {
        showCurrentOrders = false
        historyDownArrowImageView.isHidden = false
        currentOrderDownArrowImageView.isHidden = true
        self.completedOrderPage = 0
        getCompletedOrders()
    }
    
    @IBAction func currentOrderButtonTapped(_ sender: Any) {
       
        historyDownArrowImageView.isHidden = true
        currentOrderDownArrowImageView.isHidden = false
        self.currentOrderPage = 0
        getCurrentOrders()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - TableView Delegate & DataSource
extension MyOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showCurrentOrders {
            return currentOrdersList.count
        }
        return completedOrdersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(OrderTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        
        if showCurrentOrders {
            cell.configCell(isCurrentOrder: showCurrentOrders, order: currentOrdersList[indexPath.row])
        
        } else {
            cell.configCell(isCurrentOrder: showCurrentOrders, order: completedOrdersList[indexPath.row])
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var orderId = 0
        
        if showCurrentOrders {
            orderId = currentOrdersList[indexPath.row].id
            
        } else {
            orderId = completedOrdersList[indexPath.row].id
        }
        
        OrderDetails.getOrderDetails(orderId: orderId) { (data, error, message) in
            
            if error == nil {
                let controller = OrderHistoryViewController()
                controller.delegate = self
                controller.isCurrentOrder = self.showCurrentOrders
                controller.orderDetails = data
                controller.modalPresentationStyle = .overCurrentContext
                self.present(controller, animated: true)
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
}


//MARK: - RegisterComplaintDelegate
extension MyOrderViewController: RegisterComplaintDelegate, UIScrollViewDelegate {
    
    func registerComplaint(orderId: Int, driverId: Int, contractorName: String) {
        let controller = ComplaintViewController()
        controller.orderId = orderId
        controller.driverId = driverId
        controller.contractorName = contractorName
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            
            if !showCurrentOrders {
                completedOrderPage += 1
                getCompletedOrders()
                
            } else {
                currentOrderPage += 1
                getCurrentOrders()
            }
        }
    }
}
