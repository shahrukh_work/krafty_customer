//
//  RatingsPopUpViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/4/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import Cosmos

class RatingsPopUpViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var ratingDetailLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var remarksLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverImageView: UIImageView!
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        ratingView.didFinishTouchingCosmos = { rating in
            self.ratingDetailLabel.text = "You rated \(Int(rating)) star"
        }
    }
    
    
    //MARK: - Actions
    @IBAction func continueButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
