//
//  SignUpViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/22/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    //MARK: - Variables
    var enteredPassword = ""
    var enteredConfirmPassword = ""
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        bottomView.layer.cornerRadius = 18
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner]
        passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        confirmPasswordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    
    //MARK: - Actions
    @IBAction func signUpButtonTapped(_ sender: Any) {
        let userName = userNameTextField.text
        let firstName = firstNameTextField.text
        let lastName = lastNameTextField.text
        let email = emailTextField.text
        
        if userName == "" {
            self.showOkAlert("Please provide a username.")
            
        } else if firstName == "" {
            self.showOkAlert("Please provide your first name.")
            
        } else if lastName == "" {
            self.showOkAlert("Please provide your last name")
            
        } else if !Utility.isValidEmail(emailStr: email!) {
            self.showOkAlert("Please provide a valid email.")
            
        } else if enteredPassword == "" {
            self.showOkAlert("Please provide a valid password.")
            
        } else if enteredPassword != enteredConfirmPassword {
            self.showOkAlert("Passwords do not match.")
            
        } else {
            
            Profile.signUp(username: userName!, firstName: firstName!, lastName: lastName!, email: email!, password: enteredPassword) { (data, error, message) in
                
                if error == nil {
                    User.shared.profile = data
                    UserDefaults.standard.set(User.shared.profile?.toJSONString(), forKey: "UserDefaults")
                    let controller = CodeVerificationViewController()
                    controller.isForSignUp = true
                    controller.email = email!
                    self.navigationController?.pushViewController(controller, animated: true)
                
                } else {
                    self.showOkAlert(message)
                }
            }
        }
    }
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Selectors
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        
        if textField == passwordTextField {
            
            if text.count > enteredPassword.count { // entered something
                enteredPassword += String(text.last!)
                
            } else { // cleared something
                enteredPassword.removeLast()
            }
            
        } else if textField == confirmPasswordTextField {
            
            if text.count > enteredConfirmPassword.count { // entered something
                enteredConfirmPassword += String(text.last!)
                
            } else { // cleared something
                enteredConfirmPassword.removeLast()
            }
        }
        
        var stericks = ""
        for _ in 0..<text.count {
            stericks += "●"
        }
        textField.text = stericks // show stericks to the user instead of password
    }
}
