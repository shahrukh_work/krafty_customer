//
//  ForgotPasswordViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var emailTextField: UITextField!
    
    
    //MARK: ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK: - Actions
    @IBAction func nextButtonTapped(_ sender: Any) {
        let email = emailTextField.text!
        
        if !Utility.isValidEmail(emailStr: email) {
            self.showOkAlert("Please provide a valid email.")
        
        } else {
            
            PostApi.forgotPassword(email: email) { (data, error, errorCode) in
                
                if error == nil {
                    let controller = CodeVerificationViewController()
                    controller.email = email
                    self.navigationController?.pushViewController(controller, animated: true)
                
                } else {
                    self.showOkAlert("There was a problem. Please try again.")
                }
            }
        }
    }
}
