//
//  SignInViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/22/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: AppUITextField!
    
    
    //MARK: - Variables
    var enteredPassword = ""
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    
    //MARK: - Actions
    @IBAction func signInButtonTapped(_ sender: Any) {
        let email = userNameTextField.text
        
        //if !Utility.isValidEmail(emailStr: email!) || email == "" {
        if email == "" {
            self.showOkAlert("Please provide a valid email / username")
            
        } else if enteredPassword == "" {
            self.showOkAlert("Please provide a valid passwrod.")
            
        } else {
            signIn(email: email!, password: enteredPassword)
        }
    }
    
    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
        let controller = ForgotPasswordViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        let controller = SignUpViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func signIn(email: String, password: String) {
        
        Profile.signIn(email: email, password: password) { (data, error, message, statusCode) in
            
            if error == nil {
                
                if message == "DELETED" {
                    self.showOkAlert("Your user has been deleted by the admin")
                    
                } else if  message == "INACTIVE" {
                    self.showOkAlert("Your user has been deactivated by the admin")
                    
                } else if message == "PENDING" {
                    let controller = CodeVerificationViewController()
                    controller.isForSignUp = true
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }  else {
                    User.shared.profile = data
                    UserDefaults.standard.set(User.shared.profile?.toJSONString(), forKey: "UserDefaults")
                    PostApi.updateFCMToken(token: User.shared.profile?.fcmToken ?? "") { (message) in
                    }
                    Utility.setupHomeAsRootViewController()
                }
                
            } else {
                self.showOkAlert("Login Failed")
            }
        }
    }
    
    
    //MARK: - Selectors
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        
        if text.count > enteredPassword.count { // entered something
            enteredPassword += String(text.last!)
            
        } else { // cleared something
            enteredPassword.removeLast()
        }
        
        var stericks = ""
        for _ in 0..<text.count {
            stericks += "●"
        }
        textField.text = stericks // show stericks to the user instead of password
    }
}
