//
//  EditProfileViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/24/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage

class EditProfileViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var dateOfBirthField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    
    //MARK: - Variables
    var pickerView: UIPickerView!
    var genders = ["Male", "Female"]
    var selectedGender = ""
    let datePickerView:UIDatePicker = UIDatePicker()
    var imagePicker = UIImagePickerController()
    var profileImage: UIImage?
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        getUserDetials()
        imagePicker.delegate = self
        
        genderTextField.delegate = self
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: genderTextField.frame.size.width, height: genderTextField.frame.size.width))
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        genderTextField.inputView = self.pickerView
        self.pickerView.backgroundColor = .clear
        genderTextField.inputView = self.pickerView
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.maximumDate = Date()
        dateOfBirthField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControl.Event.valueChanged)
        
        let attrs = [NSAttributedString.Key.font : UIFont.font(withSize: 14),
                     NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1),
                     NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        let attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:"Change Password", attributes:attrs)
        attributedString.append(buttonTitleStr)
        changePasswordButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    private func getUserDetials(completion: (()->Void)? = nil) {
        
        Profile.getUserDetails(userId: (User.shared.profile?.profileData!.id)!) { (data,error,message) in
            
            if error == nil {
                
                if let profileData = data?.profileData {
                    self.userNameLabel.text = "\(profileData.firstName) \(profileData.lastName)"
                    self.firstNameTextField.text = profileData.firstName
                    self.lastNameTextField.text = profileData.lastName
                    self.userNameTextField.text = profileData.userName
                    self.emailTextField.text = profileData.email
                    self.dateOfBirthField.text = Utility.convertDateFormatter(date: profileData.dob, inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "MMM, d, yyyy")
                    self.phoneTextField.text = profileData.phone
                    self.genderTextField.text = profileData.gender
                    self.selectedGender = profileData.gender
                    self.profileImageView.sd_setImage(with: URL(string: APIRoutes.profileImageUrl + profileData.image), placeholderImage: UIImage(named: "male"))
                    
                    
                    User.shared.profile?.profileData?.firstName = profileData.firstName
                    User.shared.profile?.profileData?.lastName = profileData.lastName
                    User.shared.profile?.profileData?.userName = profileData.userName
                    User.shared.profile?.profileData?.email = profileData.email
                    User.shared.profile?.profileData?.dob = profileData.dob
                    User.shared.profile?.profileData?.phone = profileData.phone
                    User.shared.profile?.profileData?.gender = profileData.gender
                    User.shared.profile?.profileData?.gender = profileData.gender
                    User.shared.profile?.profileData?.image = profileData.image
                    
                    UserDefaults.standard.set(User.shared.profile?.toJSONString(), forKey: "UserDefaults")
                    completion?()
                }
            
             } else {
                self.showOkAlert("Error! Could not get user data.")
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func saveChangesButtonTapped(_ sender: Any) {
        let userName = userNameTextField.text
        let firstName = firstNameTextField.text
        let lastName = lastNameTextField.text
        let email = emailTextField.text
        let phone = phoneTextField.text
        let formattedDob = Utility.convertDateFormatter(date: dateOfBirthField.text!, inputFormat: "MMM, d, yyyy", outputFormat: "yyyy-MM-d")
        
        if userName == "" {
            self.showOkAlert("Please provide a username.")
            
        } else if firstName == "" {
            self.showOkAlert("Please provide your first name.")
            
        } else if lastName == "" {
            self.showOkAlert("Please provide your last name.")
            
        } else if !Utility.isValidEmail(emailStr: email!) {
            self.showOkAlert("Please provide a valid email.")
            
        } else if formattedDob == "" {
            self.showOkAlert("Please provide your date of birth.")
            
        } else if selectedGender == "" {
            self.showOkAlert("Please provide your gender.")
            
        } else if Utility.isPhoneNumberValid(value: phone!) {
            self.showOkAlert("Please provide a valid phone number.")
            
        } else {
            let profile = Mapper<ProfileData>().map(JSON:[:])!
            profile.id = (User.shared.profile?.profileData!.id)!
            profile.userName = userName!
            profile.firstName = firstName!
            profile.lastName = lastName!
            profile.email = email!
            profile.gender = selectedGender
            profile.dob = formattedDob
            profile.phone = phone!
            
            Utility.showLoading()
            APIClient.shared.updateProfileData(profile: profile, profileImage: profileImage) { (data, error, statusCode) in
                Utility.hideLoading()
                
                if error == nil { // success
                    
                    self.getUserDetials {
                        self.showOkAlert("Profile updated successfully")
                    }
                
                } else { // failure
                    self.showOkAlert("Something went wrong")
                }
            }
        }
    }
    
    @IBAction func changePasswordButtonTapped(_ sender: Any) {
        let controller = ChangePasswordViewController()
        controller.isForChangingPassword = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func addImageButtonTapped(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "MMM, d, yyyy"
        dateOfBirthField.text = dateFormatter.string(from: sender.date)
    }
}


//MARK: - PickerView Delegate & DataSource
extension EditProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTextField.text = genders[row]
        selectedGender = genders[row]
    }
}


extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.image = image
            profileImage = image
        }
        dismiss(animated: true, completion: nil)
    }
}
