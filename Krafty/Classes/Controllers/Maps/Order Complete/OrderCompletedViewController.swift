//
//  OrderCompletedViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/5/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OrderCompletedViewController: UIViewController {

    
    //MARK: - Variables
    var delegate: OrderCompleteDelegate?
    
    
    //MARK: - View  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK: - Actions
    @IBAction func continueButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.showRatingCard()
    }
}
