//
//  RatingView.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/4/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class RatingView: UIView {
    
    
    //MARK: - Variables
    var view: UIView!
    var index = 0
    var delegate: ViewCardsDelegate?
    var rating = 3 // default
    var remarks = ["Poor", "Satisfactory", "Good", "Very Good", "Excellent"]
    var placeHolderText = "Write something ..."
    
    
    //MARK: - Outlets
    @IBOutlet weak var remarksLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingDetailLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - View Setup
    func setup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func setupView() {
        messageTextView.text = placeHolderText
        messageTextView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        messageTextView.delegate = self
        messageTextView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        containerView.cornerRadius = 14
        containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        ratingView.didFinishTouchingCosmos = { rating in
            self.ratingDetailLabel.text = "You rated \(Int(rating)) star"
            self.rating = Int(rating)
            self.remarksLabel.text = self.remarks[self.rating-1]
        }
    }
    
    
    //MARK: - Actions
    @IBAction func minimizeButtonTapped(_ sender: Any) {
        delegate?.minimizeCard()
    }
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        delegate?.slideToNextCard(currentCardIndex: index)
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        var message = messageTextView.text
        
        if message == placeHolderText {
            message = "" // empty message if user didn't type anything
        }
        
        PostApi.rateDriver(driverId: User.shared.placeOrder.driverId, performance: remarks[rating-1], stars: rating, message: message!) { (data, error, message) in
            
            self.delegate?.slideToNextCard(currentCardIndex: self.index)
        }
    }
}


//MARK: - TextView Delegate
extension RatingView: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            textView.text = placeHolderText
            textView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        }
    }
}
