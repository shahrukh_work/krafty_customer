//
//  ServiceCollectionViewCell.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ServiceCollectionViewCell: UICollectionViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var contractorImageView: UIImageView!
    @IBOutlet weak var contractorNameLabel: UILabel!
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    //MARK: - Methods
    func configCell(service: Service) {
        contractorNameLabel.text = service.name
        contractorImageView.sd_setImage(with: URL(string: APIRoutes.serviceImageUrl + service.image), placeholderImage: UIImage(named: "male "))
        
        if service.isSelected {
            contractorImageView.borderColor = #colorLiteral(red: 0, green: 0.5294117647, blue: 1, alpha: 1)
            contractorNameLabel.font = UIFont(name: "Poppins-Bold", size: 14)
        
        } else {
            contractorImageView.borderColor = .clear
            contractorNameLabel.font = UIFont(name: "Poppins-Regular", size: 14)
        }
    }
}
