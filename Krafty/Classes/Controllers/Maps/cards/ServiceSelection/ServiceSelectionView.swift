//
//  ServiceSelectionView.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ServiceSelectionView: UIView {

    
    //MARK: - Variables
    var view: UIView!
    var lastSelectedIndex = -1
    var delegate: ViewCardsDelegate?
    var index = 0
    var services: Services? {
        didSet {
            collectionView.reloadData()
        }
    }
    
    
    //MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - View Setup
    func setup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func setupView() {
        containerView.cornerRadius = 9
        containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    
    //MARK: - Actions
    @IBAction func minimizeButtonTapped(_ sender: Any) {
        delegate?.minimizeCard()
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        delegate?.slideToNextCard(currentCardIndex: index)
    }
    
    @IBAction func CancelButtonTapped(_ sender: Any) {
        delegate?.slideToStart()
    }
}


//MARK: - CollectionView Datasource & Delegate
extension ServiceSelectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services?.servicesList.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.register(ServiceCollectionViewCell.self, indexPath: indexPath)
        cell.configCell(service: (services?.servicesList[indexPath.row])!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: (collectionView.frame.height / 2) - 22)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 00
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 44
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == lastSelectedIndex { //already selected -> de-select
            services?.servicesList[indexPath.row].isSelected = false
            nextButton.isUserInteractionEnabled = false
            nextButton.alpha = 0.6
            lastSelectedIndex = -1
        
        } else {
            
            if lastSelectedIndex == -1 { // no prior selection
                lastSelectedIndex = indexPath.row
                services?.servicesList[lastSelectedIndex].isSelected = true
                nextButton.isUserInteractionEnabled = true
                nextButton.alpha = 1
                
            } else { // select other option
                services?.servicesList[lastSelectedIndex].isSelected = false
                lastSelectedIndex = indexPath.row
                services?.servicesList[lastSelectedIndex].isSelected = true
                nextButton.isUserInteractionEnabled = true
                nextButton.alpha = 1
            }
        }
        User.shared.placeOrder.serviceId = services?.servicesList[indexPath.row].id ?? 0
        User.shared.placeOrder.serviceType = (services?.servicesList[indexPath.row].name)!
        User.shared.placeOrder.serviceImageUrl = "\(APIRoutes.serviceImageUrl)\(services!.servicesList[indexPath.row].image)"
        collectionView.reloadData()
    }
}
