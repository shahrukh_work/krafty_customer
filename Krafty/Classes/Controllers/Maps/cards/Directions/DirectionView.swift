//
//  DirectionView.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

class DirectionView: UIView {

    
    //MARK: - Variables
    var view: UIView!
    var delegate: ViewCardsDelegate?
    var index = 0
    var chatData = Mapper<ChatHistory>().map(JSON: [:])!
    
    
    //MARK: - Outlets
    @IBOutlet weak var chatStackView: UIStackView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mileView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var arrivedView: UIView!
    @IBOutlet weak var arrivalTimeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - View Setup
    func setup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func setupView() {
        messageTextView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        messageTextView.text = "Any Pickup Notes?"
        messageTextView.delegate = self
        messageTextView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        containerView.cornerRadius = 9
        containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    
    //MARK: - Actions
    @IBAction func minimizeButtonTapped(_ sender: Any) {
        delegate?.minimizeCard()
    }
    
    @IBAction func sendAction(_ sender: Any) {
        
        guard validate(textView: messageTextView) else { return }
        
        if !messageTextView.text.isEmpty || messageTextView.text != "Any Pickup Notes?" {
            let chat = Mapper<Chat>().map(JSON: [
                "id" : -1,
                "order_id" : User.shared.currentJob.id,
                "message": messageTextView.text ?? "",
                "send_by" : 1,
                "created_at" : "",
                "updated_at" : "",
            ])
            
            if let chat = chat {
                self.chatData.chatHistory.append(chat)
            }
            delegate?.sendTapped(message: messageTextView.text ?? "")
            messageTextView.text = ""
            
            self.tableView.reloadData()
            
            if !chatData.chatHistory.isEmpty {
                tableView.scrollToRow(at: IndexPath(row: chatData.chatHistory.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
    }
    
    @IBAction func arrivedAction(_ sender: Any) {
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        delegate?.slideToNextCard(currentCardIndex: index)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //MARK: - Methods
    func showContractorDetails() {
        nameLabel.text = User.shared.contractor?.name
        ratingLabel.text = "\(User.shared.contractor?.stars ?? 0)"
        personImageView.sd_setImage(with: URL(string: APIRoutes.driverImageUrl + (User.shared.contractor?.image ?? "")), placeholderImage: UIImage(named: "male"))
    }
    
    func showJobDetails() {
        nameLabel.text = User.shared.currentJob.driverName
        ratingLabel.text = "\(User.shared.contractor?.stars ?? 0)"
        personImageView.sd_setImage(with: URL(string: APIRoutes.driverImageUrl + (User.shared.currentJob.driverImage)), placeholderImage: UIImage(named: "male"))
    }
    
    func updateTimeAndDistance(time: String, distance: String) {
        arrivalTimeLabel.text = "Arriving in \(time)"
        distanceLabel.text = "\(distance) away"
    }
    
    func incommingChat(chat: Chat) {
        self.chatData.chatHistory.append(chat)
        
        if let tableView = tableView {
            tableView.reloadData()
            
            if !chatData.chatHistory.isEmpty {
                tableView.scrollToRow(at: IndexPath(row: chatData.chatHistory.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
    }
    
    func loadChatData() {
        
        Utility.showLoading()
        ChatHistory.loadChat(orderId: User.shared.currentJob.id) { (data, error, mesage)  in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.chatData = data
                    self.tableView.reloadData()
                    
                    if !self.chatData.chatHistory.isEmpty {
                            self.tableView.scrollToRow(at: IndexPath(row: self.chatData.chatHistory.count - 1, section: 0), at: .bottom, animated: false)
                    }
                }
            }
        }
    }
    
    func validate(textView: UITextView) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
            return false
        }
        return true
    }
}


//MARK: - TextView Delegate
extension DirectionView: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            messageTextViewHeightConstraint.constant = 40
            textView.text = "Any Pickup Notes?"
            textView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        }
    }
    
    //change in text view height disturbs the name and rating components in the ui so no change in the text view height should be done
//    func textViewDidChange(_ textView: UITextView) {
//        let minTextViewHeight: CGFloat = 40
//        let maxTextViewHeight: CGFloat = 80
//
//        var height = ceil(textView.contentSize.height) // ceil to avoid decimal
//
//        if (height < minTextViewHeight) {
//            height = minTextViewHeight
//        }
//
//        if (height > maxTextViewHeight) {
//            height = maxTextViewHeight
//        }
//
//        if height != messageTextViewHeightConstraint.constant {
//            messageTextViewHeightConstraint.constant = height
//            textView.setContentOffset(.zero, animated: false) // scroll to top to avoid "wrong contentOffset" artefact when line count changes
//        }
//    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension DirectionView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        chatData.chatHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chat = chatData.chatHistory[indexPath.row]
        
        if chat.sendBy == 1 {
            let cell = tableView.register(OutgoingMessageMapTableViewCell.self, indexPath: indexPath)
            cell.configCell(message: chat.message)
            cell.selectionStyle = .none
            return cell
            
        } else {
            let cell = tableView.register(IncomingMessageMapTableViewCell.self, indexPath: indexPath)
            cell.configCell(message: chat.message)
            cell.selectionStyle = .none
            return cell
        }
    }
}
