//
//  ImageCollectionViewCell.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/5/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    
    //MARK: Outlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var imageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewHeightConstraint: NSLayoutConstraint!
    var isDashedLayerAdded = false
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    //MARK: - Methods
    func config(image: UIImage, isImagePicker: Bool) {
        imageView.image = image
    }
    
    func config(imageName: String) {
        imageView.sd_setImage(with: URL(string: "\(APIRoutes.orderImageUrl)/" + imageName), placeholderImage: UIImage(named: "male "))
    }
}
