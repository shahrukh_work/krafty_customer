//
//  ServiceTypeView.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ServiceTypeView: UIView {

    
    //MARK: - Variables
    var view: UIView!
    var selectionsMade = false
    var delegate: ViewCardsDelegate?
    var index = 0
    var subServices: SubServices? {
        didSet {
            collectionView.reloadData()
        }
    }
    
    
    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var contractorImageView: UIImageView!
    @IBOutlet weak var contractorTypeLabel: UILabel!
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - View Setup
    func setup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupView()
        //loadData()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func setupView() {
        containerView.cornerRadius = 9
        containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    
    //MARK: - Private Method
    private func updateView() {
        selectionsMade = false
        
        for subService in subServices!.subServicesList {
            selectionsMade = selectionsMade || subService.isSelected
        }
        
        if selectionsMade {
            nextButton.alpha = 1
            nextButton.isUserInteractionEnabled = true
            
        } else {
            nextButton.alpha = 0.6
            nextButton.isUserInteractionEnabled = false
        }
    }

    
    //MARK: - Actions
    @IBAction func minimizeButtonTapped(_ sender: Any) {
        delegate?.minimizeCard()
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        delegate?.slideToNextCard(currentCardIndex: index)
    }
    
    @IBAction func CancelButtonTapped(_ sender: Any) {
        delegate?.slideToStart()
    }
}


//MARK: - CollectionView Datasource & Delegate
extension ServiceTypeView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subServices?.subServicesList.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.register(ServiceTypeCollectionViewCell.self, indexPath: indexPath)
        cell.configure(subService: (subServices?.subServicesList[indexPath.row])!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.text = subServices?.subServicesList[indexPath.row].name
        label.sizeToFit()
        return CGSize(width: label.frame.width + 50, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        subServices?.subServicesList[indexPath.row].isSelected.toggle()
        collectionView.reloadData()
        updateView()
    }
}
