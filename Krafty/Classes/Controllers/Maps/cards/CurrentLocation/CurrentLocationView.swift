//
//  CurrentLocationView.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/28/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class CurrentLocationView: UIView {

    
    //MARK: - Variables
    var view: UIView!
    var delegate: ViewCardsDelegate?
    var index = 0
    
    
    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    //MARK: - View Setup
    func setup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func setupView() {
        containerView.cornerRadius = 9
        currentLocationButton.cornerRadius = 9
        currentLocationButton.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.26), opacity: 1.0, radius: 0.5)
    }
    
    
    //MARK: - Actions
    @IBAction func currentLocationPressed(_ sender: Any) {
        delegate?.slideToNextCard(currentCardIndex: index)
    }
}
