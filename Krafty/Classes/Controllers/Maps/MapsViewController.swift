//
//  MapsViewController.swift
//  Krafty
//
//  Created by Bilal Saeed on 4/24/20.
//  Copyright © 2020 Bilal Saeed. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import GooglePlaces
import IQKeyboardManagerSwift
import SocketIO
import ObjectMapper


class MapsViewController: UIViewController {
    
    
    //MARK: - Variables
    private let locationManager = CLLocationManager()
    var driverMarker = GMSMarker()
    var myPositionMarker = GMSMarker()
    var sourceView: UIView?
    var placesClient = GMSPlacesClient()
    var searchTerm = ""
    var selectedPlace: GMSPlace!
    var searchResults = [GMSAutocompletePrediction]()
    var sessionToken = GMSAutocompleteSessionToken()
    var isLoadingData = false
    let cardSize = [174, 422, 388, 564, 104, 245, 564]
    let currentLocationView = CurrentLocationView()
    var serviceSelectionView = ServiceSelectionView()
    var serviceTypeView = ServiceTypeView()
    var serviceDetailsConfirmView = ServiceDetailsConfirmView()
    var findingServiceProviderView = FindingServiceProviderView()
    var directionView = DirectionView()
    var ratingView = RatingView()
    var cardsViewList = [UIView]()
    let pulsator = Pulsator()
    var polyline = GMSPolyline()
    var manager:SocketManager!
    var currentCardIndex = 0
    var didOnce = false
    var isFirsttime = true
    
    //MARK: - Outlets
    @IBOutlet weak var notificationRedView: UIView!
    @IBOutlet weak var mapsView: GMSMapView!
    @IBOutlet weak var cardContainerView: UIView!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTextField: SearchTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendMessageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var recenterLocationButton: UIButton!
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bellButton: UIButton!
    @IBOutlet weak var hamburgerButton: UIButton!
    @IBOutlet weak var minimizedView: UIView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupSocks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirsttime {
            isFirsttime.toggle()
            slideExitInsert(view: cardsViewList[0], height: cardSize[0], exitLeft: true)
        }
        locationSetup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    //MARK: - View Setup
    func setupView() {
        getServicesList()
        loadCardViews()
        minimizedView.layer.cornerRadius = 12
        minimizedView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        driverMarker.icon = #imageLiteral(resourceName: "driverMarker")
        driverMarker.map = mapsView
        
        myPositionMarker.icon = #imageLiteral(resourceName: "ic_pin")
        myPositionMarker.map = mapsView
        
        tableView.dataSource = self
        tableView.delegate = self
        
        searchTextField.setLeftImage(image: #imageLiteral(resourceName: "search"))
        Utility.setPlaceHolderTextColor(searchTextField, "Search", #colorLiteral(red: 0.6392156863, green: 0.6352941176, blue: 0.6745098039, alpha: 1))
        
        if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
            
            do {
                mapsView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                
            } catch {
                print(error.localizedDescription)
            }
        } else {
            NSLog("Unable to find MapStyle.json")
        }
        setupPulsator()
    }
    
    func locationSetup() {
        locationManager.delegate = self
        
        switch(CLLocationManager.authorizationStatus()) {
            
        case .restricted, .denied:
            self.showOkAlertWithOKCompletionHandler("Application uses location data for navigations and maps. Kindly enable location services in order to use the application.") { ( _) in
                UIApplication.shared.open(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            }
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        default:
            break
        }
    }
    
    private func setupPulsator() {
        pulsator.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.6078431373, blue: 0.937254902, alpha: 1)
        pulsator.numPulse = 3
        pulsator.radius = 240.0
        myPositionMarker.iconView?.transform = CGAffineTransform.init(translationX: 0, y: 200)
        myPositionMarker.iconView?.cornerRadius = 100
        myPositionMarker.iconView?.clipsToBounds = true
        myPositionMarker.iconView?.layer.addSublayer(pulsator)
        pulsator.position = CGPoint(x: 100, y: 100)
    }
    
    private func loadCardViews() {
        currentLocationView.index = 0 // 0
        currentLocationView.delegate = self
        cardsViewList.append(currentLocationView)
        
        reInitializeViews()
    }
    
    private func reInitializeViews() {
        //after completing the order, when we order again, the states of cards selected earlier do not change. so we have to initialize the views again.
        
        //if count > 5, it means the cards have been initialized once, so we have to 're-initialize' them. We will remove them first.
        if cardsViewList.count > 5 {
            cardsViewList.remove(at: 6)
            cardsViewList.remove(at: 5)
            cardsViewList.remove(at: 4)
            cardsViewList.remove(at: 3)
            cardsViewList.remove(at: 2)
            cardsViewList.remove(at: 1)
            
            serviceSelectionView = ServiceSelectionView()
            serviceTypeView = ServiceTypeView()
            serviceDetailsConfirmView = ServiceDetailsConfirmView()
            findingServiceProviderView = FindingServiceProviderView()
            directionView = DirectionView()
            ratingView = RatingView()
        }
        
        //if count < 5 it means that the cards have not been initialized before.
        serviceSelectionView.index = 1
        serviceSelectionView.delegate = self
        cardsViewList.append(serviceSelectionView)
        
        serviceTypeView.index = 2
        serviceTypeView.delegate = self
        cardsViewList.append(serviceTypeView)
        
        serviceDetailsConfirmView.index = 3
        serviceDetailsConfirmView.delegate = self
        serviceDetailsConfirmView.parentController = self
        cardsViewList.append(serviceDetailsConfirmView)
        
        findingServiceProviderView.index = 4
        findingServiceProviderView.delegate = self
        cardsViewList.append(findingServiceProviderView)
        
        directionView.index = 5
        directionView.delegate = self
        cardsViewList.append(directionView)
        
        ratingView.index = 6
        ratingView.delegate = self
        cardsViewList.append(ratingView)
    }
    
    
    //MARK: - Actions
    @IBAction func searhTextChanged(_ sender: Any) {
        
        tableView.isHidden = searchTextField.text == "" ? true : false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            if self?.searchTerm == self?.searchTextField.text && !(self?.isLoadingData ?? false) {
                self?.searchResults.removeAll()
                self?.tableView.reloadData()
                self?.isLoadingData = true
                self?.fetchLocations(query: self?.searchTerm ?? "")
            }
        }
        
        searchTerm = searchTextField.text ?? ""
    }
    
    @IBAction func hamburgerPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func bellButtonTapped(_ sender: Any) {
        let controller = NotifiationsViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func recenterLocationButtonTapped(_ sender: Any) {

        let lat = myPositionMarker.position.latitude
        let lng = myPositionMarker.position.longitude
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 15)
        getAddressFromLatLon()
        searchTextField.text = ""
        locationManager.startUpdatingLocation()
        self.mapsView.animate(to: camera)
    }
    
    @IBAction func maximizeCardButtonTapped(_ sender: Any) {
        cardContainerView.isHidden = false
        minimizedView.isHidden = true
    }
    
    
    //MARK: - Private Methods
    private func draw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        let driverLocation = "\(src.latitude),\(src.longitude)"
        let orderLocation = "\(dst.latitude),\(dst.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(driverLocation)&destination=\(orderLocation)&mode=driving&key=\(kGoogleMapsAPI)"
        print(url)
        
        Alamofire.request(url).responseJSON { response in
            
            do {
                self.polyline.map = nil //remove last polyline from map
                self.driverMarker.position = src
                let json = try JSON(data: response.data!)
                let routes = json["routes"].arrayValue
                
                let distanceTime = json["routes"].arrayValue.first
                
                if let distanceTime = distanceTime {
                    let leg = distanceTime["legs"].arrayValue.first
                    
                    if let leg = leg {
                        let distance = leg["distance"].dictionaryObject?["text"]
                        let time = leg["duration"].dictionaryObject?["text"]
                        
                        if let distance = distance as? String, let time = time as? String {
                            self.directionView.updateTimeAndDistance(time: time, distance: distance)
                        }
                    }
                }
                
                for route in routes {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    
                    self.polyline = GMSPolyline(path: path)
                    self.polyline.strokeColor = .black
                    self.polyline.strokeWidth = 2
                    self.polyline.map = self.mapsView
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func getAddressFromLatLon() {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let geoCoder = GMSGeocoder()
        center.latitude = myPositionMarker.position.latitude
        center.longitude = myPositionMarker.position.longitude
        
        geoCoder.reverseGeocodeCoordinate(center) { (response, error) in
            
            if error == nil {
                
                if let response = response {
                    
                    if let pm = response.firstResult() {
                        
                        var addressString : String = ""
                        
                        if let name = pm.thoroughfare {
                            addressString = addressString + name + ", "
                        }
                        
                        if let subLocality = pm.subLocality {
                            addressString = addressString + subLocality + ", "
                        }
                        
                        if let locality = pm.locality {
                            addressString = addressString + locality + ", "
                        }
                        
                        if let admin = pm.administrativeArea {
                            addressString = addressString + admin + ", "
                        }
                        
                        if let country = pm.country {
                            addressString = addressString + country
                        }
                        User.shared.placeOrder.address = addressString
                        User.shared.placeOrder.location = "\(self.myPositionMarker.position.latitude),\(self.myPositionMarker.position.longitude)"
                        self.currentLocationView.locationLabel.text = addressString
                    }
                }
            }
        }
    }
    
    private func slideExitInsert(view: UIView, height: Int, exitLeft: Bool = true) {
        let width = cardContainerView.frame.width
        
        var multiplier:CGFloat = 1.1
        
        if !exitLeft {
            multiplier = -1.1
        }
        
        let insertFrame = CGRect(x: multiplier * width, y: 0.0, width: width, height: CGFloat(height))
        
        if let exitView = cardContainerView.subviews.first {
            
            if exitView == view {
                return
            }
            
            var y: CGFloat = 0.0
            let exitViewHeight = exitView.frame.height
            
            if exitViewHeight < CGFloat(height) {
                y = CGFloat(height) - exitViewHeight
                exitView.frame = CGRect(x: 0, y: y, width: width, height: exitViewHeight)
            }
            
            UIView.animate(withDuration: 0.4, animations: { [weak self] in
                self?.viewHeightConstraint.constant = CGFloat(height)
                
                exitView.frame = CGRect(x: -multiplier * width, y: y, width: width, height: exitViewHeight)
                
            }) { ( _) in
                exitView.removeFromSuperview()
            }
        }
        
        viewHeightConstraint.constant = CGFloat(height)
        view.frame = insertFrame
        cardContainerView.addSubview(view)
        
        UIView.animate(withDuration: 0.4) {
            view.frame = CGRect(x: 0, y: 0.0, width: width, height: CGFloat(height))
        }
    }
    
    private func fetchLocations(query: String) {
        
        placesClient.findAutocompletePredictions(fromQuery: query, filter: nil, sessionToken: sessionToken) { (data, error) in
            
            if error == nil {
                
                if let results = data {
                    self.searchResults = results
                    self.tableView.reloadData()
                    self.isLoadingData = false
                }
                
            } else {
                self.showOkAlert(error?.localizedDescription ?? "")
            }
        }
    }
    
    private func fetchLocationDetails(placeID: String) {
        
        placesClient.fetchPlace(fromPlaceID: placeID, placeFields: .coordinate, sessionToken: sessionToken) { (data, error) in
            
            if error == nil {
                self.selectedPlace = data
                
                if let dta = data {
                    let cameraPosition = GMSCameraPosition.camera(withLatitude: dta.coordinate.latitude, longitude: dta.coordinate.longitude, zoom: 15.0)
                    self.mapsView.animate(to: cameraPosition)
                    self.myPositionMarker.position = dta.coordinate
                    self.locationManager.stopUpdatingLocation()
                    self.getAddressFromLatLon()
                }
                
            } else {
                self.showOkAlert(error?.localizedDescription ?? "")
            }
        }
    }
    
    private func enableIQKeyboard() {
        IQKeyboardManager.shared.enable = true
        NotificationCenter.default.removeObserver(self)
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    private func disableIQKeyboard() {
        registerKeyboardNotifications()
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
    }
    
    private func showHideSearchComponents(hideComponents: Bool) {
        
        if searchTextField.isHidden == hideComponents { // return if already hidden
            return
        }
        recenterLocationButton.isHidden = hideComponents
        searchTextField.isHidden = hideComponents
        searchBackView.isHidden = hideComponents
    }
    
    private func showHideNavBarButtons(hideButtons: Bool, title: String) {
        titleLabel.text = title
        
        // if we want to hide the components and they are already hidden: return 'or' if we want to unhide the components and they are already visible: return
        if hamburgerButton.isHidden == hideButtons {
            return
        }
        notificationRedView.isHidden = hideButtons
        bellButton.isHidden = hideButtons
        hamburgerButton.isHidden = hideButtons
    }
    
    private func getServicesList() {
        
        Services.getServicesList() { (data, error, message) in
            
            if error == nil {
                
                if data?.servicesList.count == 0 {
                    self.showOkAlert("No services are available at the moment.")
                
                } else {
                    self.serviceSelectionView.services = data
                    User.shared.services = data!
                    self.slideExitInsert(view: self.cardsViewList[self.currentCardIndex+1], height: self.cardSize[self.currentCardIndex+1])
                    
                }
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
    
    private func getSubServicesList(serviceId: Int) {
        
        SubServices.getSubServices(serviceId: serviceId) { (data, error, message) in
            
            if error == nil {
                
                if data?.subServicesList.count == 0 {
                    self.showOkAlert("No sub-services are available at the moment.")
                
                } else {
                    self.serviceTypeView.subServices = data
                    self.enableIQKeyboard()
                    self.showHideSearchComponents(hideComponents: true)
                    self.polyline.map = nil
                    self.showHideNavBarButtons(hideButtons: false, title: "")
                    self.slideExitInsert(view: self.cardsViewList[self.currentCardIndex+1], height: self.cardSize[self.currentCardIndex+1])
                }
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
    
    private func placeOrder() {
        let placedOrder = User.shared.placeOrder
        
        Utility.showLoading()
        APIClient.shared.placeOrder(placedOrder: placedOrder) { (data, error, statusCode) in
            Utility.hideLoading()
            
            if error == nil {
                self.slideExitInsert(view: self.cardsViewList[self.currentCardIndex+1], height: self.cardSize[self.currentCardIndex+1])
                
            } else {
                self.showOkAlert(kErrorMessage)
            }
        }
    }
    
    private func cancelOrder() {
        let driverId = User.shared.contractor!.id > 0 ? String(User.shared.contractor!.id) : ""
        // if driverId > 0 it means some driver has accepted the job so we will pass driverId along with orderId
        // if driverId == "" it means the app was in search of some driver at this point so we will send empty driverId along with orderId
        PostApi.cancelOrder(orderId: User.shared.currentJob.id, driverId: driverId) { (data, error, message) in
            
            if error == nil {
                self.enableIQKeyboard()
                self.slideToStart()
                self.polyline.map = nil
                self.showHideSearchComponents(hideComponents: false)
                self.showHideNavBarButtons(hideButtons: false, title: "")
                
            } else {
                print("Error! Order could not be cancelled.")
            }
        }
    }
    
    
    //MARK: - Sockets
    private func setupSocks() {
        manager = SocketManager(socketURL: URL(string: APIRoutes.socketEndPoint)!, config: [.log(true), .compress, .secure(true), .extraHeaders(["Authorization" : "bearer \(User.shared.profile?.token ?? "")"])])
        User.shared.socket = manager.defaultSocket
        User.shared.socket.connect()
        
        User.shared.socket.on(clientEvent: .connect) { (data, error) in
            User.shared.socket.emit("join-customer", ["customer_id": "\(User.shared.profile?.profileData?.id ?? -1)"])
        }
        
        User.shared.socket.on(clientEvent: .error) { (data, ackEmitter) in
            print(data)
        }
        
        self.socksListenEvents()
    }
    
    private func socksListenEvents() {
        
        // if a job was already in progress before closing the app, this channel will listen to update the UI
        User.shared.socket.on("current-job") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                
                if let data = Mapper<Job>().map(JSONObject: socketData) {
                    
                    // some contractor has accepted the job
                    if data.driverId != 0 {
                        User.shared.currentJob = data
                        self.directionView.showJobDetails()
                        self.directionView.loadChatData()
                        self.currentCardIndex = 5
                        self.disableIQKeyboard()
                        self.showHideSearchComponents(hideComponents: true)
                        self.showHideNavBarButtons(hideButtons: true, title: "Searching")
                        self.slideExitInsert(view: self.cardsViewList[self.currentCardIndex], height: self.cardSize[self.currentCardIndex])
                    
                    // this else is executed if the socket has been reconnected and no contracted has accepted the job yet. so we have to move the card to findingServiceProviderView
                    } else {
                            
                        let serice = User.shared.services.servicesList.filter { (item) -> Bool in
                            if item.id == data.serviceType {
                                return true
                            }
                            return false
                        }
                        self.findingServiceProviderView.findingContractorLabel.text = serice.first?.name ?? ""
                        data.serviceTypeString = serice.first?.name ?? ""
                        User.shared.currentJob.id = data.id
                        self.showHideSearchComponents(hideComponents: true)
                        self.showHideNavBarButtons(hideButtons: true, title: "Searching")
                        self.currentCardIndex = 4
                        self.slideExitInsert(view: self.cardsViewList[self.currentCardIndex], height: self.cardSize[self.currentCardIndex])
                    }
                    
                } else {
                    //self.showOkAlert("Something went wrong!.")
                }
            }
        }
        
        // this channel listen once the contractor accepts a job
        User.shared.socket.on("job-accepted") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                print (socketData)
                
                if let contractor = Mapper<Contractor>().map(JSONObject: socketData) {
                    User.shared.contractor = contractor
                    self.directionView.showContractorDetails()
                    self.currentCardIndex = 5
                    self.disableIQKeyboard()
                    self.showHideSearchComponents(hideComponents: true)
                    self.showHideNavBarButtons(hideButtons: true, title: "")
                    self.slideExitInsert(view: self.cardsViewList[self.currentCardIndex], height: self.cardSize[self.currentCardIndex])
                }
                
            } else {
                print("data not found")
            }
        }
        
        // this channel listens to driver's location
        User.shared.socket.on("location") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                if let location = Mapper<ContractorLocation>().map(JSONObject: socketData) {
                    User.shared.contractor?.location = location
                    
                    let driverLocation = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                    let orderLocation = CLLocationCoordinate2D(latitude: Double(User.shared.currentJob.lat) ?? 0.0, longitude: Double(User.shared.currentJob.long) ?? 0.0)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                        self.draw(src: driverLocation, dst: orderLocation)
                    }
                }
                
            } else {
                print("data not found")
            }
        }
        
        // this channel listens to the incoming messages
        User.shared.socket.on("add-message") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                
                if let data = Mapper<Chat>().map(JSONObject: socketData) {
                    self.directionView.incommingChat(chat: data)
                    
                } else {
                    print("error parsing chat")
                }
            }
        }
        
        // this channel listens if contractor has arrived
        User.shared.socket.on("Arrived") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                print(socketData)
                self.showHideSearchComponents(hideComponents: true)
                self.showHideNavBarButtons(hideButtons: true, title: "Arrived")
            }
        }
        
        // this channel listens if the contractor has finished the job
        User.shared.socket.on("job-finish") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                print(socketData)
                self.slideToStart()
                let controller = OrderCompletedViewController()
                controller.delegate = self
                controller.modalPresentationStyle = .overCurrentContext
                self.present(controller, animated: true, completion: nil)
            }
        }
        
        // this channel listens if the contractor has canceled the job
        User.shared.socket.on("job-cancel") { (sockData, ackEmitter) in
            
            if let socketData = sockData.first {
                print(socketData)
                self.slideToStart()
                self.showHideNavBarButtons(hideButtons: false, title: "")
                self.showHideSearchComponents(hideComponents: false)
                self.showOkAlert("Your order was cancelled by the service provide.")
            }
        }
    }
}


//MARK: - CLLocationManagerDelegate
extension MapsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        mapsView.isMyLocationEnabled = true
        mapsView.settings.myLocationButton = true
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {
            return
        }
        
        if !didOnce {
            mapsView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            didOnce = true
        }
        myPositionMarker.position = location.coordinate
        getAddressFromLatLon()
        pulsator.start()
    }
}


//MARK: - TableView DataSource & Delegate
extension MapsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(GoogleLocationAutoCompleteTableViewCell.self, indexPath: indexPath)
        cell.configure(data: searchResults[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fetchLocationDetails(placeID: searchResults[indexPath.row].placeID)
        searchTextField.attributedText = searchResults[indexPath.row].attributedPrimaryText
        sessionToken = GMSAutocompleteSessionToken()
        self.tableView.isHidden = true
        enableIQKeyboard()
        showHideSearchComponents(hideComponents: true)
        polyline.map = nil
        showHideNavBarButtons(hideButtons: false, title: "")
        getServicesList()
    }
}


//MARK: - Keyboard Observers
extension MapsViewController {
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
                
        if keyboardSize.height > 200  && sendMessageViewBottomConstraint.constant < 100 {
            
            UIView.animate(withDuration: 0.5) {
                self.directionView.mileView.isHidden = true
                self.directionView.tableView.isHidden = false
                self.sendMessageViewBottomConstraint.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        if keyboardSize.height > 200  && sendMessageViewBottomConstraint.constant > 100 {
            
            UIView.animate(withDuration: 0.5) {
                self.directionView.mileView.isHidden = false
                self.directionView.tableView.isHidden = true
                self.sendMessageViewBottomConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
}


//MARK: - ViewCardsDelegate
extension MapsViewController: ViewCardsDelegate {
    
    func slideToNextCard(currentCardIndex: Int) {
        
        self.currentCardIndex = currentCardIndex
        serviceTypeView.contractorImageView.sd_setImage(with: URL(string: User.shared.placeOrder.serviceImageUrl), placeholderImage: UIImage(named: "male "))
        serviceDetailsConfirmView.contractorImageView.sd_setImage(with: URL(string: User.shared.placeOrder.serviceImageUrl), placeholderImage: UIImage(named: "male "))
        serviceDetailsConfirmView.locationLabel.text = User.shared.placeOrder.address
        serviceTypeView.contractorTypeLabel.text = User.shared.placeOrder.serviceType
        serviceDetailsConfirmView.contractorTypeLabel.text = User.shared.placeOrder.serviceType
        findingServiceProviderView.findingContractorLabel.text = "Finding \(User.shared.placeOrder.serviceType)"
        
        switch currentCardIndex {
            
        case 1: // called from ServiceSelectionView
            getSubServicesList(serviceId: User.shared.placeOrder.serviceId)
            break
        
        case 2: // called from ServiceTypeView
            enableIQKeyboard()
            showHideSearchComponents(hideComponents: true)
            polyline.map = nil
            showHideNavBarButtons(hideButtons: false, title: "")
            slideExitInsert(view: cardsViewList[currentCardIndex+1], height: cardSize[currentCardIndex+1])
            break
        
        case 3: // called from ServiceDetailsConfirmView
            enableIQKeyboard()
            showHideSearchComponents(hideComponents: true)
            polyline.map = nil
            showHideNavBarButtons(hideButtons: true, title: "Searching")
            placeOrder()
            break
        
        case 4: // called from FindingServiceProviderView
            cancelOrder()
            break
        
        case 5: // called from DirectionView
            cancelOrder()
            break
            
        case 6: // called from RatingView
            enableIQKeyboard()
            slideToStart()
            polyline.map = nil
            showHideNavBarButtons(hideButtons: false, title: "")
            break
        
        default: // called from CurrentLocationView
            enableIQKeyboard()
            showHideSearchComponents(hideComponents: true)
            polyline.map = nil
            showHideNavBarButtons(hideButtons: false, title: "")
            getServicesList()
            break
        }
    }
    
    func slideToStart() {
        reInitializeViews()
        slideExitInsert(view: cardsViewList[0], height: cardSize[0])
        showHideSearchComponents(hideComponents: false)
        showHideNavBarButtons(hideButtons: false, title: "")
        //clear marker and route from the map if finished/cancelled
        self.driverMarker.map = nil
        self.polyline.map = nil
    }
    
    func minimizeCard() {
        cardContainerView.isHidden = true
        minimizedView.isHidden = false
    }
    
    func sendTapped(message: String) {
        
        User.shared.socket.emit("chat", [
            "id": User.shared.currentJob.driverId > 0 ? User.shared.currentJob.driverId : User.shared.contractor!.id,
            "order_id": User.shared.currentJob.id,
            "message": message,
            "send_by": 1
        ])
    }
}


//MARK: - OrderCompleteDelegate
extension MapsViewController: OrderCompleteDelegate {
    func showRatingCard() {
        User.shared.placeOrder = PlaceOrder() // re-initialize the object to clear previous details
        showHideSearchComponents(hideComponents: true)
        showHideNavBarButtons(hideButtons: true, title: "Arrived")
        slideExitInsert(view: cardsViewList[6], height: cardSize[6])
    }
}
