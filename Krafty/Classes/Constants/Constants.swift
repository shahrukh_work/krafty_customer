//
//  Constants.swift
//  ShaeFoodDairy
//
//  Created by Bilal Saeed on 3/17/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

let kApplicationWindow = Utility.getAppDelegate()!.window
let kGoogleMapsAPI = "AIzaSyDOm8ZLmF-Nl5OEWlKafBBD8s3Ze3pFwjA"
let kErrorMessage = "There was a problem. Please try again."

func googleDirectionURL(origin: String, destination: String) -> String {
    return "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGoogleMapsAPI)"
}

class APIRoutes {
    static let baseUrl = "https://krafty-backend.codesorbit.com"
    static let create = "/api/customer/user/create"
    static let verifyPin = "/api/customer/user/verifyPin"
    static let resendVerificationPin = "/api/customer/user/resendVerificationPin"
    static let signin = "/api/authorization/auth/customerLogin"
    static let getUserDetials = "/api/customer/user/getUserById"
    static let updateProfile = "/api/customer/user/update"
    static let profileImageUrl = "\(APIRoutes.baseUrl)/api/customer/customerImages"
    static let forgotPassword = "/api/authorization/auth/forgotPassword"
    static let updatePassword = "/api/authorization/auth/updatePassword"
    static let changePassword = "/api/authorization/auth/changePassword"
    static let getServicesList = "/api/customer/service/listServices"
    static let getSubServices = "/api/customer/service/getSubServices"
    static let setNotificationStatus = "/api/customer/setting/setNotificationStatus"
    static let getNotificationStatus = "/api/customer/setting/getNotificationStatus"
    static let registerComplaint = "/api/customer/complain/registerComplain"
    static let getCurrentOrders = "/api/customer/order/getCurrentOrders"
    static let getCompletedOrders = "/api/customer/order/getOrderHistory"
    static let cancelOrder = "/api/customer/order/cancelOrder"
    static let rateDriver = "/api/customer/order/rateDriver"
    static let getOrderChatHistory = "/api/customer/order/getOrderChatHistory"
    static let orderImageUrl = "\(APIRoutes.baseUrl)/api/customer/orderImage"
    static let serviceImageUrl = "\(APIRoutes.baseUrl)/api/admin/serviceImages"
    static let placeOrder = "/api/customer/order/placeOrder"
    static let getOrderDetails = "/api/customer/order/getOrderDetail"
    static let driverImageUrl = "\(APIRoutes.baseUrl)/api/driver/driverImages"
    static let logout = "/api/authorization/auth/customerLogout"
    static let socketEndPoint = "wss://krafty-backend.codesorbit.com"
    static let updateFCMToken = "/api/customer/user/updateFcmToken"
    static let getNotifications = "/api/customer/notification/getNotifications"
    static let clearNotifications = "/api/customer/notification/clearNotication"
    static let readNotifications = "/api/customer/notification/readNotication"
}
